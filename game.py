from random import randint
from unicodedata import name

name = input("Hi! What is your name? ")

yay = "I knew it!"
nay = "Drat! Lemme try again!"
end = "That's all for my guesses."

guesses = range(1,6)
for i in guesses:
    mm = str(randint(1,12))
    yyyy = str(randint(1942,2004))

    question = name + " were you born in " + mm + "/" + yyyy + "?"

    print("Guess " + str(i) + ": " + question)
    answer = input("yes or no? ")
    print("Answer:", answer)

    if answer == "yes":
        print(yay)
        break
    elif i == 5:
        print(end)
    else:
        print(nay)